import React, { Component } from 'react'
import { render } from 'react-dom'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"

import Courses from './components/courses'
import Quiz from './components/quiz'

const App = () => {
  return(
		<Router>
			<div className="vh-100">
				<div className="row h-100">
					<div className="col-2 bg-dark">
						<nav className="nav flex-column">
							<Link to="/courses" className="nav-item">Courses</Link>
							<Link to="/quiz" className="nav-item">Quiz</Link>
							<Link to="/something" className="nav-item">Something</Link>
						</nav>
					</div>
					<div className="col">
						<Route path="/courses"><Courses/></Route>
						<Route path="/quiz"><Quiz/></Route>
					</div>
				</div>
			</div>
		</Router>
  )
}

render(<App />, document.getElementById('app'))
