# Basic React App

Use parcel-bundler for bundling front-end code

```js
npm i -g parcel-bundler (gives global access to parcel)
```

run with 
```js
parcel serve src/index.html
```
